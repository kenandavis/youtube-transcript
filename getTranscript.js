var casper = require('casper').create();
var fs = require('fs');


// read in csv file and convert to json
var records = (function () {
	var csv, rows, headers, records;

	csv = fs.read( 'data/political_ads.csv' );
	rows = csv.trim().split( '\n' ).filter( Boolean ).map( function ( str ) {
		return str.split( ',' );
	});

	headers = rows.shift();

	records = rows.map( function ( row ) {
		var record = {};

		row.forEach( function ( value, i ) {
			record[ headers[i] ] = value;
		});

		return record;
	});

	return records;
}());



// loop thru ads and save transcripts
casper.start().each(records, function(self, ad) {
	
	console.log(ad.url, ad.type, ad.state, ad.politician, ad.title);

	self.userAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36');

	// load website and print title
	self.thenOpen(ad.url, function() {
		this.echo(this.getTitle());
	});

	// click on more button
	self.wait(2000, function() {
		this.click("#action-panel-overflow-button");
		console.log('clicking more button...');
	});

	// save image showing current state where more is clicked
	self.wait(2000, function() {
		this.capture('images/more.png');
	});

	// click transcript button
	self.wait(2000, function() {
		this.click("button[data-trigger-for='action-panel-transcript']");
		console.log('clicking transcript button...');
	});

	// save image of state where transcript is loaded onto the page
	self.wait(2000, function() {
		this.capture('images/transcript.png');
	});

	// fetch the text
	self.wait(2000, function() {
		//var text = this.fetchText(".caption-line-text"); // prints out text from muliple page elements
		//console.log(text);

		var lines = this.evaluate(function() {

			__utils__.echo('getting lines...'); // prints the console inside an evaluate method
	        var elements = __utils__.findAll('.caption-line-text');
	        return Array.prototype.map.call(elements, function(e) {
	            return e.textContent;
	        });
	    });

		// concatenate lines with new line
	    var transcript_string = lines.join('\n');

	    var output_name = ad.state.toLowerCase().replace(" ", "_") + "_" + ad.type.toLowerCase().replace(" ", "_") + "_" + ad.politician.toLowerCase().replace(" ", "_") + "_" + ad.title.toLowerCase().replace(" ", "_");

		// save transcript to a text file
	 	fs.write('scripts/'+output_name+'.txt', transcript_string, 'w'); 
	 
	 });	

});



casper.run();



